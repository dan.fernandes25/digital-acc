import { Notify } from 'quasar'
export default {
  methods: {
    NotifyDanger (message) {
      Notify.create({
        color: 'negative',
        position: 'top',
        message: message,
        icon: 'report_problem'
      })
    },

    NotifySuccess (message) {
      Notify.create({
        color: 'positive',
        position: 'top',
        message: message,
        icon: 'check_circle'
      })
    },
    NotifySuccessNoClose (message) {
      Notify.create({
        color: 'positive',
        position: 'top',
        message: message,
        closeBtn: true,
        timeout: 0,
        icon: 'check_circle'
      })
    },

    NotifyInfo (message) {
      Notify.create({
        color: 'primary',
        position: 'top',
        message: message,
        icon: 'info'
      })
    },

    NotifyWarning (message) {
      Notify.create({
        color: 'warning',
        position: 'top',
        message: message,
        icon: 'warning'
      })
    }
  }
}
