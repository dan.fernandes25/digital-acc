import mask from 'vue-the-mask'

export default async ({ Vue }) => {
  Vue.use(mask)
}
