import axios from 'axios'

const $axios = axios.create({
  baseURL: process.env.BASE_URL
})
$axios.interceptors.request.use(function (config) {
  let token = localStorage.getItem('token')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
    return config
  }
  return config
}, function (error) {
  return Promise.reject(error)
})
export const service = $axios
