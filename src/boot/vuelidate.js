import vuelidate from 'vuelidate'

export default async ({ Vue }) => {
  Vue.use(vuelidate)
}
