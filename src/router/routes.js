
const routes = [
  {
    path: '/',
    component: () => import('layouts/Login.vue'),
    children: [
      { path: '', component: () => import('pages/auth/login.vue') }
    ]
  },
  {
    path: '/home',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'Dashboard', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/transacao',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', name: 'Transação', component: () => import('pages/transaction/transactionList.vue') },
      { path: 'novaTransacao', name: 'Cadastro de Transação', component: () => import('pages/transaction/newTransaction.vue') },
      { path: 'editar/:id', name: 'Editar Transação', component: () => import('pages/transaction/newTransaction.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
