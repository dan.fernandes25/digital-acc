const statusResponse = (obj, type, description) => {
  let response = JSON.parse(JSON.stringify(obj)).response
  return !navigator.onLine ? new Error('Sem acesso à internet, por favor, tente mais tarde') : (!response ? new Error('Servidor indisponivel, tente mais tarde') : verifyStatusResponse(response, type, description))
}
const verifyStatusResponse = (response, type, description) => {
  let data = response.data
  let status = response.status
  switch (status) {
    case 400:
      return verifyErrorsData(data)
    case 401:
      return new Error('Sessão expirada, por favor , logue novamente')
    case 404:
      return Error('Endereço de acesso não encontrado')
    case 500:
      return verifyTypeAction(type, description)
    case 503:
      return Error(`Servidor indisponivel, tente mais tarde`)
  }
}
const verifyErrorsData = (data) => {
  console.log(data)
  return data ? (data.length > 1 ? data : new Error(data.errorMessage[0].errorMessage)) : new Error('Existem erros a serem processados')
}
const verifyTypeAction = (action, description) => {
  let typesAction = {
    'list': { error: new Error(`Não foi possível realizar a listagem, tente mais tarde`) },
    'get': { error: new Error(`Não foi possível realizar a consulta de ${description}, tente mais tarde`) },
    'create': { error: new Error('Não foi possível realizar o cadastro, tente mais tarde') },
    'update': { error: new Error('Não foi possível realizar a edição, tente mais tarde') },
    'remove': { error: new Error('Não foi possível realizar a exclusão, tente mais tarde') },
    'report': { error: new Error('Não foi possível realizar a geração do relatório, tente mais tarde') },
    'login': { error: new Error('Não foi possível realizar o login, tente mais tarde') }
  }
  return typesAction[action].error
}
export {
  statusResponse
}
