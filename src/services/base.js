import { service } from '../boot/axios'
import { statusResponse } from './responseService'
export default class Base {
  constructor (api) {
    this.api = api
    this.service = service
    this.statusResponse = statusResponse
  }
    list = ($params) => {
      return new Promise((resolve, reject) => {
        service.get(this.api, { params: $params }).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'list'))
        })
      })
    }
    get = ($id) => {
      return new Promise((resolve, reject) => {
        service.get(`${this.api}/${$id}`).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'get', 'item'))
        })
      })
    }

    create = ($data) => {
      return new Promise((resolve, reject) => {
        service.post(this.api, $data).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'create'))
        })
      })
    }

    update = ($data) => {
      return new Promise((resolve, reject) => {
        service.put(this.api, $data).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'update'))
        })
      })
    }

    remove = ($id) => {
      return new Promise((resolve, reject) => {
        service.delete(`${this.api}/${$id}`).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'remove'))
        })
      })
    }

    patch = ($id) => {
      return new Promise((resolve, reject) => {
        service.patch(`${this.api}/${$id}`).then((response) => {
          resolve(response)
        }).catch((err) => {
          reject(statusResponse(err, 'update'))
        })
      })
    }
    // reportPdfGet = (api, $params) => {
    //   return new Promise((resolve, reject) => {
    //     service.get(api, { params: $params, headers: { Accept: 'application/pdf' }, responseType: 'arraybuffer' }).then((response) => {
    //       resolve(response)
    //     }).catch((err) => {
    //       reject(statusResponse(err, 'report'))
    //     })
    //   })
    // }
    // reportExcelGet = (api, $params) => {
    //   return new Promise((resolve, reject) => {
    //     service.get(api, { params: $params, headers: { Accept: 'application/excel' }, responseType: 'arraybuffer' }).then((response) => {
    //       resolve(response)
    //     }).catch((err) => {
    //       reject(statusResponse(err, 'report'))
    //     })
    //   })
    // }
}
