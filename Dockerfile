FROM estevaobraga/w3as:quasar
WORKDIR /app
COPY . .
RUN yarn install
ENTRYPOINT ["quasar", "dev"]
EXPOSE 8080